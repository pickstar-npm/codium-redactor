## Redactor

A Vue wrapper component for Redactor.

### Installation

Add package via Yarn:

```
$ yarn add git+ssh://git@code.codium.com.au:vue-components/redactor.git
```

Import default styles to your global `scss` file for the project:

```
@import "codium-redactor/dist/codium-redactor.css";
```

Overwrite the styles as necessary.

### Usage

Import the component where you need it:

```
import Redactor from 'codium-redactor'
```

Define it in your components list:

```
components: {
  Redactor
}
```

Use it in your templates:

```
<redactor v-model="description" name="description" ></redactor>
```

#### Options

##### `disabled`

Type: `boolean`
Default: `false`

Disables content editing and hides the toolbar if set to true.

##### `placeholder`

Type: `string`
Default: `' '`

Add some placeholder text

#### Install Component Globally

You can install the component globally so it does not need to be done for every component you use it. In a main file:

```
import Vue from 'vue'
import Redactor from 'codium-redactor'

Vue.component('redactor', Redactor)
```
