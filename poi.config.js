module.exports = {
  entry: './src/CodiumRedactor.vue',
  filename: {
    js: 'codium-redactor.js',
    css: 'codium-redactor.css'
  },
  sourceMap: false,
  html: false,
  format: 'cjs'
}
